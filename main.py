import random
from termcolor import colored

players = []

dict = {"blue": 1, "magenta": 2, "red": 3, "yellow": 4, "green": 5, "white" : 0}
dict2 = {"blue": "🃠", "magenta": "🃢", "red": "🃣", "yellow": "🃥", "green": "🃬", "white" : "🂿"}

def get_key(dict, key):
    return list(dict.keys())[list(dict.values()).index(key)]

class Card:

    color = None

    def __init__(self, color):
        self.color = color

    def __eq__(self, other):
        return self.color == other.color


cards = []
for i in range(5):
    for j in range(7):
        cards.append(Card(get_key(dict, i + 1)))
cards.append(Card(get_key(dict, 5)))


class Player:
    name = None
    seat = 0
    score = 0
    state = "play"

    def __init__(self, name, seat):
        self.name = name
        self.seat = seat
        self.hand = []
        self.score = 0
        self.state = "play"


def get_cards():
    new_cards = cards.copy()
    random.shuffle(new_cards)
    a = len(new_cards) // len(players)
    for player in players:
        player.hand = []
        for i in range(a):
            player.hand.append(new_cards.pop())
        player.hand.sort(key=lambda x: x.color)



def round_finished():
    for player in players:
        player.state = "play"
        player.score += len(player.hand)
        if len(player.hand) == 0:
            player.score += -2
            if player.score < 0:
                player.score = 0
    players.append(players.pop(0))


class PinguPartyRound:

    def __init__(self):
        self.state_player = [player.state for player in players]
        self.board = []
        for i in range(8):
            line = []
            for j in range(15 - i):
                line.append(0)
            self.board.append(line)
        get_cards()
        end_state = []
        for player in players:
            end_state.append("pass")
        while self.state_player != end_state:
            for player in players:
                self.player_turn(player)
            self.state_player = [player.state for player in players]

        round_finished()

    def player_turn(self, Player):
        if Player.state == "play":
            add = self.show_board()
            self.show_points()
            self.show_cards(Player)
            aktion = input(Player.name + " Bitte Name einer Karte oder passen: ")
            if aktion == "passen":
                self.passen(Player)
            else:
                color = aktion
                if Card(color) in Player.hand:
                    i = Player.hand.index(Card(color))
                    self.play_card(Player, Player.hand[i], add)
                    return
                else:
                    print("Wähle eine andere Karte oder passe:")
                    self.player_turn(Player)
                    return

    def passen(self, Player):
        Player.state = "pass"

    def play_card(self, Player, Card, add):
        place = input("Wo möchten sie die Karte hinspielen:")
        if place == "undo":
            self.player_turn(Player)
            return
        else:
            try:
                x = int(place[:-1]) + add
                y = int(place[-1])
            except:
                print("Zahlen bitte")
                self.play_card(Player, Card, add)
                return
            if not 0 <= x <= (14-y):
                print("Zu weit außen")
                self.play_card(Player, Card, add)
                return
            if not 0 <= y <= 7:
                print("Diese Etage gibt es nicht")
                self.play_card(Player, Card, add)
                return
            if self.board[y][x] == 0:
                if y == 0:
                    if self.board[0][7] == 0:
                        self.board[0][7] = dict[Card.color]
                    elif self.board[0].count(0) >= 8:
                        left = 0
                        right = 0
                        try:
                            left = self.board[0][x - 1]
                        except:
                            pass
                        try:
                            right = self.board[0][x + 1]
                        except:
                            pass
                        if left != 0 or right != 0:
                            self.board[0][x] = dict[Card.color]
                        else:
                            print("Bitte benachbart legen")
                            self.play_card(Player, Card, add)
                            return
                    else:
                        print("Unten ist voll!")
                        self.play_card(Player, Card, add)
                else:
                    down_left = self.board[y - 1][x]
                    down_right = self.board[y - 1][x + 1]
                    if down_left != 0 and down_right != 0:
                        if down_left == dict[Card.color] or down_right == dict[Card.color]:
                            self.board[y][x] = dict[Card.color]
                        else:
                            print("Diese Farbe darf hier nicht liegen")
                            self.play_card(Player, Card, add)
                            return
                    else:
                        print("Karten können nicht schweben!")
                        self.play_card(Player, Card, add)
                        return
            else:
                print("Da liegt schon eine Karte!")
                self.play_card(Player, Card, add)
                return
            Player.hand.remove(Card)

    def show_board(self):
        first = next((i for i, x in enumerate(self.board[0]) if x), 7)
        list = self.board[0].copy()
        list.reverse()
        last = next((i for i, x in enumerate(list) if x), 7)
        last = 14 - last
        for i in range(8):
            print(" " * (18 - i), end='')
            for c, j in enumerate(self.board[-1 - i]):
                if last - 7 > c:
                    print("  ", end='')
                elif 14 - (7 - i) - (7 - first) < c:
                    print("  ", end='')
                else:
                    print(colored(dict2[get_key(dict, j)], get_key(dict, j)), end=' ')
            print()
        add = last - 7
        return add

    def show_points(self):
        for player in players:
            print(player.name + ' : ' + str(player.score), end=' ')
        print()

    def show_cards(self, Player):
        for i in range(len(Player.hand)):
            text = colored(Player.hand[i].color, Player.hand[i].color)
            print(text, end=' ')
        print()
        return



class PinguParty:
    done = False

    def __init__(self):
        while not self.done:
            PinguPartyRound()
            for player in players:
                if player.score >= 10:
                    self.done = True
        players.sort(key=lambda x: x.score)
        if players[0].score == players[1].score:
            print("Die Gewinner sind {} und {} mit {} Punkten!".format(players[0].name, players[1].name, players[0].score))
        else:
            print("Der Gewinner ist {} mit {} Punkten!".format(players[0].name, players[0].score))
        for player in players:
            print(player.name + ' : ' + str(player.score))


if __name__ == "__main__":
    while True:
        names = input("Geben Sie bitte die Spielernamen ein: ")
        yes = input("Ist dies korrekt: " + names + " [y/n] ")
        if yes == 'y':
            names = names.strip()
            namelist = names.split(",")
            for c, name in enumerate(namelist):
                players.append(Player(name, c))
            break

    game = PinguParty()
